import React, { Component } from 'react';
import superagent from 'superagent';
import Icon from '@material-ui/core/Icon';
import AddCircle from '@material-ui/icons/AddCircle'
import Button from './Button'



class Users extends Component {
	constructor() {
		super()

		this.state = {
			users: []
		};

	}

	componentDidMount() {
		superagent
			.get('https://goodtreatments.herokuapp.com/api/v1/users')
			.then(data => {
				let allUsers = data.body.data
				this.setState({
					users: allUsers
				});
			})
	}


	render() {
		const { classes } = this.props;

		return (
			<div>
				<table>
					<tr>
						<th>Id</th>
						<th>Name</th>
						<th>email</th>
					</tr>
					{this.state.users.map(user => {
						return (
							<tr>
								<th>{user._id}</th>
								<th>{user.name}</th>
								<th>{user.email}</th>
							</tr>
						)
					})}
				</table>
				<Button />
			</div>
		);
	}
}


export default Users;