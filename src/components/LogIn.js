import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import superagent from 'superagent';

class LogIn extends Component {
  constructor(props) {
    super(props)

    this.state = {
      showLogin: true,
      showRecord: false,
      name: '',
      email: ''
    }
  }

  onSubmithandle = e => {
    e.preventDefault()
    if (!this.state.showLogin) {

      const json = {
        name: this.state.name,
        email: this.state.email
      }
    }

  }
  //va mostrando los valores que se van ingresando en el input
  onChangeInput = event => {
    const { id, value } = event.target

    console.log(id);
    console.log(value);

    this.setState({
      [id]: value
    })
  }


  login = (e) => {
    this.setState({
      showLogin: !this.state.showLogin,
      showRecord: !this.state.showRecord
    })
  }


  onSubmitHandle = e => {
    e.preventDefault()

    if (!this.state.showLogin) {

      const json = {
        name: this.state.name,
        email: this.state.email
      }
      console.log(json);

      superagent.post('https://goodtreatments.herokuapp.com/api/v1/users')
        .send(json)
        .then(res => {
          console.log(res.body.data);
          alert('User Saved!')
        })
        .catch(e => alert(e))

    }

    // this.setState({
    //   showLogin: !this.state.showLogin,
    // })

  }

  render() {

    const formStyles = {
      width: 400,
      margin: '50px auto',
    }

    return (
      <React.Fragment>
        {
          this.state.showLogin &&
          <div style={formStyles}>
            <form onSubmit={this.login}>
              <TextField
                required
                name="email"
                label="Email"
                fullWidth
                onChange={this.onChangeInput}
              />
              <TextField
                required
                name="password"
                type="password"
                label="Password"
                fullWidth
                onChange={this.onChangeInput}
              />
              <Button type='submit' variant='contained'>Login</Button>
            </form>
          </div>
        }
        {
          this.state.showRecord &&
          <div style={formStyles}>
            <form onSubmit={this.onSubmitHandle}>
              <TextField
                required
                id="name"
                name="name"
                label="name"
                fullWidth
                onChange={this.onChangeInput}
              />
              <TextField
                required
                id="email"
                name="email"
                label="email"
                fullWidth
                onChange={this.onChangeInput}
              />
              <Button type='submit' variant='contained'>Record</Button>
            </form>
          </div>
        }

      </React.Fragment>
    );
  }
}

export default LogIn;