import React, { Component } from 'react';
import superagent from 'superagent'

class Treatments extends Component {
	constructor() {
		super()

		this.state = {
			treatments: []
		};

	}

	componentDidMount() {
		superagent
			.get('https://goodtreatments.herokuapp.com/api/v1/treatments')
			.then(data => {
				let allUsers = data.body.data
				this.setState({
					treatments: allUsers
				});
			})
	}


	render() {

		return (
			<div>
				<table>
					<tr>
						<th>User Id</th>
						<th>Descriptions</th>
						<th>Treatments</th>
						<th>Appointments</th>
					</tr>
					{this.state.treatments.map(treatments => {
						return (
							<tr>
								<th>{treatments._id}</th>
								<th>{treatments.descriptions}</th>
								<th>{treatments.listOfTreatments}</th>
								<th>{treatments.listOfAppointments}</th>
							</tr>
						)
					})}
				</table>
			</div>
		);
	}
}


export default Treatments;